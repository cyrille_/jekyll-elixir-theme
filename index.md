---
layout: index
title: ELIXIR Luxembourg
section: ELIXIR Luxembourg
permalink: /
introduction: >-
  <p>ELIXIR-LU, the Luxembourgish node of ELIXIR, the European infrastructure for life science information, focuses on long-term sustainability of tools and data for Translational Medicine.</p>
  <p>Translational Medicine data integrate clinical information with molecular and cellular data for a better understanding of diseases.</p>
  <p>ELIXIR-LU aims to facilitate long-term access to those research data and to tools for scientists in both academia and industry. This will allow the reuse of previously generated data to address new research questions and dramatically save time and cost.</p>

---

## Services

**Repository for high quality Translational Medicine data**

Integration of well-curated clinical and molecular data from cohorts and large consortia. Implementation of standardised electronic data capture, data harmonisation and curation.

----

**High-performance data access and computing services**

Sustainable access management according to well-defined criteria, in order to meet the security and accountability requirements of ELIXIR, of the General European data protection regulation (GDPR), and of the research community.  Platforms and tools to allow efficient data access and analysis.

---

<a href="{{ '/sustainability-data/' | relative_url }}"><picture>
    <source media="(max-width: 600px)" srcset="{{ '/assets/buttons/data_catalog_small.jpg' | relative_url }}" />
    <source media="(min-width: 600px)" srcset="{{ '/assets/buttons/data_catalog.jpg' | relative_url }}" />
    <img src="{{ '/assets/buttons/data_catalog.jpg' | relative_url }}" alt="Data catalog" style="width:155px;" />
  </picture></a>
<a href="{{ '/sustainability-tools/' | relative_url }}"><picture>
    <source media="(max-width: 600px)" srcset="{{ '/assets/buttons/tools_registry_small.jpg' | relative_url }}" />
    <source media="(min-width: 600px)" srcset="{{ '/assets/buttons/tools_registry.jpg' | relative_url }}" />
    <img src="{{ '/assets/buttons/tools_registry.jpg' | relative_url }}" alt="Tools Registry" style="width:155px;" />
</picture></a>
<a href="{{ '/gdpr-activities/' | relative_url }}"><picture>
    <source media="(max-width: 600px)" srcset="{{ '/assets/buttons/gdpr_small.jpg' | relative_url }}" />
    <source media="(min-width: 600px)" srcset="{{ '/assets/buttons/gdpr.jpg' | relative_url }}" />
    <img src="{{ '/assets/buttons/gdpr.jpg' | relative_url }}" alt="GDPR activities" style="width:155px;" />
  </picture></a>


---

## Support and user training

Workshops and courses on data management, curation, analytics and visualisation. Continuous education of software developers, data managers and researchers.
