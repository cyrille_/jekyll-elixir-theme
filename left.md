---
layout: left_col
title: Collaborations
permalink: /left
section: About
pages:
  - title: ELIXIR-LU Team
    href: /about-node/team
  - title: Collaborations
    href: /about-node/collaborations
  - title: Funding and governance
    href: /about-node/funding-and-governance
  - title: Vacancies
    href: /about-node/vacancies
---

ELIXIR-LU is always looking to add new national and international collaborations with partners from public and private sectors in Luxembourg and abroad. The Node especially aims to interact with the pharmaceutical industry and the clinical healthcare.

On a national scale, the Node aims to unify the Luxembourgish biomedical IT landscape and implement international data standards. The local biomedical research institutions will be able to use the ELIXIR portal to store and promote the data produced by their cohorts and biological sample collections.

Internationally, ELIXIR-LU intends to become a leading stakeholder in biomedicine, as a secure data hub and a high-performance computing location. The Node will host data from different European and international consortia. For example, ELIXIR-LU participated within IMI-eTRIKS in the initial setup and metadata collection of IMI (Innovative Medicines Initiative) projects.  ELIXIR-LU will help sustain data that emerged from these projects beyond the end of the funded project duration. Both the volume and the scope of data available from such international projects will increase over the years, making the Node’s <a href="http://datacatalog.elixir-luxembourg.org/">data catalogue</a> of interest to researchers worldwide.

ELIXIR-LU has also developed strong collaborations within the ELIXIR infrastructure, including with the Hub and many other Nodes (including but not limited to: ELIXIR-UK, ELIXIR-Denmark, ELIXIR-Spain, ELIXIR-Czech Republic and ELIXIR-Finland).
