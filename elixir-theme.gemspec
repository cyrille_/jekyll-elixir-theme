# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.name          = "jekyll-elixir-theme"
  spec.version       = "0.9.30"
  spec.authors       = ["jacek.lebioda"]
  spec.email         = ["jacek.lebioda@uni.lu"]

  spec.summary       = "Jekyll theme for ELIXIR blog site"
  spec.homepage      = "https://git-r3lab.uni.lu/elixir/jekyll-elixir-theme"
  spec.license       = "Apache2"

  spec.files         = `git ls-files -z`.split("\x0").select { |f| f.match(%r!^(assets|_layouts|_includes|_sass|LICENSE|README)!i) }

  spec.add_runtime_dependency "jekyll", "~> 4.0"

  spec.add_development_dependency "bundler", "~> 2.0"
  spec.add_development_dependency "rake", "~> 12.0"
end
